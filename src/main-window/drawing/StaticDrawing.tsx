import React, { MouseEvent, useState, useEffect } from "react";
import BrushOptions from "./BrushOptions";
import { PixelDisplay } from "../pixels/PixelDisplay";
import { emptyGrid } from "../pixels/PixelUtils";
import config from "../../Config";
import { Box, Button } from "@mui/material";
import { PixelValues, RGB } from "../../types/types";
import useWindowDimensions from "../../hooks/useWindowDimensions";
import { Brush, defaultBrush } from "./Brush";
import { useUndo } from "use-undoable-state";
import { Redo, Undo } from "@mui/icons-material";

export default function StaticDrawing() {
  const { width } = useWindowDimensions();
  const pixelSize: number = Math.max(3, Math.floor(width / 128));
  const [pixels, setPixels, undoPixels, redoPixels] = useUndo<PixelValues>(
    emptyGrid()
  );
  const [brush, setBrush] = useState<Brush>(defaultBrush);
  const [paletteColors, setPaletteColors] = useState<RGB[]>([
    { r: 0, g: 0, b: 0 },
  ]);

  useEffect(() => {
    fetch(`${config.baseUrl}/draw`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        pixels,
      }),
    }).catch((error) => {
      console.error("Error:", error);
    });
  }, [pixels]);

  const updatePalette = (colour: RGB) => {
    if (!paletteColors.includes(colour)) {
      setPaletteColors([colour, ...paletteColors].slice(0, 9));
    }
  };

  const drawPixelIfMouseIsDown = (e: MouseEvent<HTMLCanvasElement>) => {
    if (e.buttons % 2 === 1) {
      brush.drawFunction({
        event: e,
        setPixels,
        brush,
        pixels,
        pixelSize,
      });
      updatePalette(brush.colour);
    }
  };

  return (
    <Box
      sx={{
        justifyItems: "center",
        flexDirection: "column",
        alignItems: "center",
        display: "flex",
      }}
    >
      <PixelDisplay
        key={"static.display"}
        pixelSize={pixelSize}
        pixels={pixels}
        onMouseDown={drawPixelIfMouseIsDown}
        onMouseMove={drawPixelIfMouseIsDown}
      />

      <Box sx={{ display: "flex", justifyContent: "center" }}>
        <Box>
          <Button onClick={undoPixels}>
            <Undo />
          </Button>
          <Button onClick={redoPixels}>
            <Redo />
          </Button>
        </Box>
        <BrushOptions
          brush={brush}
          setBrush={setBrush}
          brushPalette={paletteColors}
        />
      </Box>
    </Box>
  );
}

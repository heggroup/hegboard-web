import { PixelValues, RGB } from "../../types/types";

export function emptyGrid(): PixelValues {
  const result = [];
  const blackPixel = { r: 0, g: 0, b: 0 };

  for (let i = 0; i < 128; i++) {
    const col = [];
    for (let j = 0; j < 64; j++) {
      col.push(blackPixel);
    }
    result.push(col);
  }

  return result;
}

export function updatePixel(
  oldPixels: PixelValues,
  x: number,
  y: number,
  colour: RGB
) {
  return updatePixels(oldPixels, [{ x, y, colour }]);
}

export function updatePixels(
  oldPixels: PixelValues,
  newPixels: { x: number; y: number; colour: RGB }[]
): PixelValues {
  const newValues: PixelValues = [];

  oldPixels.forEach((row) => {
    const newRow: RGB[] = [];
    row.forEach((pixel) => {
      newRow.push(pixel);
    });
    newValues.push(newRow);
  });

  newPixels.forEach((newPixel) => {
    newValues[newPixel.x][newPixel.y] = newPixel.colour;
  });

  return newValues;
}

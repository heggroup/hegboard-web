# HegBoard Web

HegBoard Web is the web app designed to interact with the [HegBoard Project](https://gitlab.com/L-Heggerz/hegboard).

## Project setup

Install the dependencies with:

### `npm install`

Run the tests with:

### `npm test`

Run the project locally at [http://localhost:3000](http://localhost:3000) with:

### `npm start`

To build a static directory run:

### `npm run build`

## Versioning

Changes on master are automatically versioned and released.
 - Pushes directly to master are interpreted as patch changes.
 - Merge requests are interpreted as minor changes.
 - Overwriting the version in `package.json` to an non-existant version will override both of the previous behaviours.
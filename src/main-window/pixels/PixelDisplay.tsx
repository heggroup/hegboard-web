import React, { MouseEventHandler, useEffect, useRef } from "react";
import config from "../../Config";
import { PixelValues, toCssColor } from "../../types/types";

interface PixelDisplayArguments {
  pixelSize: number;
  pixels: PixelValues;
  onMouseDown?: MouseEventHandler | undefined;
  onMouseMove?: MouseEventHandler | undefined;
  onMouseUp?: MouseEventHandler | undefined;
}

export function drawPixels(
  canvas: HTMLCanvasElement,
  pixels: PixelValues,
  pixelSize: number,
  enableGrid: boolean
) {
  const context = canvas.getContext("2d", { alpha: false });
  const drawPixelSize = enableGrid ? pixelSize - 1 : pixelSize;
  if (context) {
    context.fillStyle = "#555555";
    context.fillRect(0, 0, context.canvas.width, context.canvas.height);
    for (let x = 0; x < config.width; x++) {
      for (let y = 0; y < config.height; y++) {
        const colour = pixels[x][y];
        context.fillStyle = toCssColor(colour);
        context.fillRect(
          x * pixelSize,
          y * pixelSize,
          drawPixelSize,
          drawPixelSize
        );
        context.fill();
      }
    }
  }
}

export function PixelDisplay({
  pixelSize,
  pixels,
  onMouseDown,
  onMouseMove,
  onMouseUp,
}: PixelDisplayArguments) {
  const canvasRef = useRef<HTMLCanvasElement>(null);
  const enableGrid = pixelSize > 2;

  useEffect(() => {
    const canvas = canvasRef?.current;
    if (canvas) {
      drawPixels(canvas, pixels, pixelSize, pixelSize > 2);
    }
  }, [pixels, pixelSize]);

  const width = enableGrid
    ? pixelSize * config.width
    : pixelSize * config.width - 1;
  const height = enableGrid
    ? pixelSize * config.height
    : pixelSize * config.height - 1;

  return (
    <canvas
      width={width}
      height={height}
      ref={canvasRef}
      onMouseDown={onMouseDown}
      onMouseMove={onMouseMove}
      onMouseUp={onMouseUp}
    />
  );
}

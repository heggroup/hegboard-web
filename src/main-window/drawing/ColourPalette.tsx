import { Box } from "@mui/material";
import React from "react";
import { RGB, toCssColor } from "../../types/types";

interface PalletteArguments {
  colors: RGB[];
  width: number;
  height: number;
  onClick: (c: RGB) => void;
}

const ColourTile = (props: { color: RGB; onClick: (c: RGB) => void }) => {
  return (
    <Box sx={{ padding: 0.5 }} onClick={() => props.onClick(props.color)}>
      <Box
        sx={{
          background: toCssColor(props.color),
          padding: 1,
          border: 2,
          borderColor: "grey.300",
        }}
      />
    </Box>
  );
};

const ColourPalette = ({
  colors,
  width,
  height,
  onClick,
}: PalletteArguments) => (
  <>
    {Array(height)
      .fill("")
      .map((_, x) => (
        <Box
          key={`colour-pallete-column-${x}`}
          sx={{
            flexDirection: "inline",
            display: "flex",
            px: 1,
            alignItems: "center",
          }}
        >
          {Array(width)
            .fill("")
            .map((_, y) => (
              <ColourTile
                key={`colour-pallete-tile-${y}`}
                color={colors[x * height + y] ?? { r: 0, g: 0, b: 0 }}
                onClick={onClick}
              />
            ))}
        </Box>
      ))}
  </>
);

export default ColourPalette;

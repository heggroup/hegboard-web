export type RGB = { r: number; g: number; b: number };

export type PixelValues = Array<Array<RGB>>;

export function toCssColor({ r, g, b }: RGB) {
  return `rgb(${r}, ${g}, ${b})`;
}

import { PlaylistAdd, PlaylistPlay } from "@mui/icons-material";
import {
  Box,
  Divider,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
} from "@mui/material";
import React, { CSSProperties, useEffect, useState } from "react";
import { FixedSizeList } from "react-window";

export default function AnimationQueue() {
  return (
    <>
      <ListItem button key="queue label">
        <ListItemIcon>
          <PlaylistPlay />
        </ListItemIcon>
        <ListItemText primary="Animation Queue" />
      </ListItem>
      <ScrollableAnimQueue />
      <ListItem button key="add">
        <ListItemIcon>
          <PlaylistAdd />
        </ListItemIcon>
        <ListItemText primary="Add Animation" />
      </ListItem>
    </>
  );
}

type AnimationQueueItem = {
  image?: string;
  name: string;
};

type RenderRowProps = {index: number, style: CSSProperties};

function ScrollableAnimQueue() {
  const [listHeight, setListHeight] = useState(
    window.innerHeight - 48 * 3 - 64
  );

  useEffect(() => {
    window.addEventListener("resize", () => {
      setListHeight(window.innerHeight - 48 * 3 - 64);
    });
  });

  const animations: AnimationQueueItem[] = [
    {
      name: "Animation 1",
    },
    {
      name: "Animation 2",
    },
  ];

  const renderRow = ({index, style}: RenderRowProps ) => {
    const animationInfo = animations[index];

    return (
      <>
        <ListItem style={style} key={index} component="div" disablePadding>
          <ListItemButton>
            <ListItemText
              id={animationInfo.name}
              primary={animationInfo.name}
            />
          </ListItemButton>
        </ListItem>
        <Divider />
      </>
    );
  };

  return (
    <Box
      sx={{
        width: "100%",
        height: listHeight,
        maxWidth: 240,
        bgcolor: "background.paper",
      }}
    >
      <FixedSizeList
        height={listHeight}
        width={360}
        itemSize={46}
        itemCount={animations.length}
        overscanCount={5}
      >
        {renderRow}
      </FixedSizeList>
    </Box>
  );
}

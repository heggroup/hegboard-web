import { MouseEvent } from "react";
import { PixelValues } from "../../types/types";
import { updatePixels } from "../pixels/PixelUtils";
import { Brush } from "./Brush";
import { drawPixelCircle, drawPixelSquare } from "./PaintingUtils";

export interface DrawFunctionArgs {
  event: MouseEvent<HTMLCanvasElement>;
  setPixels: (pixels: PixelValues) => void;
  brush: Brush;
  pixelSize: number;
  pixels: PixelValues;
}

export const drawCircle = ({
  event,
  setPixels,
  brush,
  pixelSize,
  pixels,
}: DrawFunctionArgs) => {
  const canvas = event.target as HTMLCanvasElement;
  const rect = canvas.getBoundingClientRect();
  const clientX = event.clientX - rect.x;
  const clientY = event.clientY - rect.y;

  const updatedPixels = [
    ...drawPixelCircle(
      { x: clientX, y: clientY },
      brush.size / 2,
      pixelSize,
      brush.colour
    ),
  ];

  setPixels(updatePixels(pixels, updatedPixels));
};

export const drawSquare = ({
  event,
  setPixels,
  brush,
  pixelSize,
  pixels,
}: DrawFunctionArgs) => {
  const canvas = event.target as HTMLCanvasElement;
  const rect = canvas.getBoundingClientRect();
  const clientX = event.clientX - rect.x;
  const clientY = event.clientY - rect.y;

  const updatedPixels = [
    ...drawPixelSquare(
      { x: clientX, y: clientY },
      brush.size / 2,
      pixelSize,
      brush.colour
    ),
  ];

  setPixels(updatePixels(pixels, updatedPixels));
};

export const brushShapes = [drawCircle, drawSquare];

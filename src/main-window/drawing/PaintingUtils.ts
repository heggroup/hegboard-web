import config from "../../Config";
import { RGB } from "../../types/types";

type Point = {
  x: number;
  y: number;
};

function gradient(p1: Point, p2: Point): number {
  return (p2.y - p1.y) / (p2.x - p1.x);
}

function distance(p1: Point, p2: Point): number {
  return Math.sqrt(Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2));
}

export function drawPixelCircle(
  center: Point,
  radius: number,
  pixelSize: number,
  colour: RGB
): { x: number; y: number; colour: RGB }[] {
  const updatedPixels: { x: number; y: number; colour: RGB }[] = [];
  const centerPixel = {
    x: Math.floor(center.x / pixelSize),
    y: Math.floor(center.y / pixelSize),
  };

  for (
    let x: number = Math.floor(centerPixel.x - radius);
    x < centerPixel.x + radius;
    x++
  ) {
    for (
      let y: number = Math.floor(centerPixel.y - radius);
      y < centerPixel.y + radius;
      y++
    ) {
      if (
        0 <= x &&
        x < 128 &&
        0 <= y &&
        y < 64 &&
        distance(
          { x: x * pixelSize, y: y * pixelSize },
          { x: center.x, y: center.y }
        ) <
          radius * pixelSize
      ) {
        updatedPixels.push({ x, y, colour: colour });
      }
    }
  }

  return updatedPixels;
}

export function drawPixelLine(
  lineStart: Point,
  lineEnd: Point,
  thickness: number,
  pixelSize: number,
  colour: RGB
) {
  const updatedPixels: { x: number; y: number; colour: RGB }[] = [];

  const lineStartPixel = {
    x: lineStart.x / pixelSize,
    y: lineStart.y / pixelSize,
  };
  const lineEndPixel = {
    x: lineEnd.x / pixelSize,
    y: lineEnd.y / pixelSize,
  };

  const m = gradient(lineStartPixel, lineEndPixel) || 0;
  const c1 = lineStartPixel.y - lineStartPixel.x * m;

  console.log(m);

  const pixelThickness = Math.ceil(thickness);

  const minX = Math.min(lineStartPixel.x, lineEndPixel.x) - pixelThickness;
  const maxX = Math.max(lineStartPixel.x, lineEndPixel.x) + pixelThickness;
  const minY = Math.min(lineStartPixel.y, lineEndPixel.y) - pixelThickness;
  const maxY = Math.max(lineStartPixel.y, lineEndPixel.y) + pixelThickness;

  for (let x: number = minX; x < maxX; x++) {
    for (let y: number = minY; y < maxY; y++) {
      if (0 <= x && x < config.width && 0 <= y && y < config.height) {
        const c2 = y - x / m;
        // Trust me?
        const intersectX = (c2 - c1) / (2 * m);
        const intersectY = (c2 + c1) / 2;
        console.log(intersectX);
        console.log(intersectY);
        if (
          distance(
            { x: x * pixelSize, y: y * pixelSize },
            { x: intersectX, y: intersectY }
          ) <
          thickness * pixelSize
        ) {
          updatedPixels.push({ x, y, colour: colour });
        }
      }
    }
  }

  return updatedPixels;
}

export function drawPixelSquare(
  center: Point,
  width: number,
  pixelSize: number,
  colour: RGB
): { x: number; y: number; colour: RGB }[] {
  const updatedPixels: { x: number; y: number; colour: RGB }[] = [];
  const centerPixel = {
    x: Math.floor(center.x / pixelSize),
    y: Math.floor(center.y / pixelSize),
  };

  for (
    let x: number = Math.ceil(centerPixel.x - width);
    x < Math.ceil(centerPixel.x + width);
    x++
  ) {
    for (
      let y: number = Math.ceil(centerPixel.y - width);
      y < Math.ceil(centerPixel.y + width);
      y++
    ) {
      updatedPixels.push({ x, y, colour: colour });
    }
  }

  return updatedPixels;
}

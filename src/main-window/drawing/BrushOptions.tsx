import { Box, Input, MenuItem, Select, Typography } from "@mui/material";
import React, { Dispatch, SetStateAction } from "react";
import { RGB } from "../../types/types";
import { Brush, hexToRgb, rgbToHex } from "./Brush";
import ColourPalette from "./ColourPalette";
import { brushShapes } from "./DrawFunctions";

interface BrushOptionsArgs {
  brush: Brush;
  setBrush: Dispatch<SetStateAction<Brush>>;
  brushPalette: RGB[];
}

const columnStyle = {
  flexDirection: "column",
  display: "flex",
  px: 2,
  alignItems: "flex-start",
  justifyContent: "center",
};

export default function BrushOptions({
  brush,
  setBrush,
  brushPalette,
}: BrushOptionsArgs) {
  return (
    <Box
      sx={{
        flexDirection: "inline",
        display: "flex",
        px: 1,
        alignItems: "top",
      }}
    >
      <Box sx={columnStyle}>
        <Box sx={{ padding: 1, display: "flex", alignItems: "center" }}>
          <Typography>Colour:</Typography>
          <Input
            type="color"
            id="brushcolor"
            name="brushColor"
            value={rgbToHex(brush.colour)}
            onChange={(e) =>
              setBrush({ ...brush, colour: hexToRgb(e.target.value) })
            }
            sx={{ width: 20, mx: 1 }}
          />
        </Box>
        <ColourPalette
          colors={brushPalette}
          width={3}
          height={3}
          onClick={(c) => setBrush({ ...brush, colour: c })}
        />
      </Box>
      <Box sx={columnStyle}>
        <Box
          sx={{
            padding: 1,
            flexDirection: "inline",
            display: "flex",
            alignItems: "center",
          }}
        >
          <Typography>Size:</Typography>
          <Input
            type="number"
            id="brushsize"
            name="brushSize"
            value={brush.size}
            sx={{ width: "2em", mx: 1 }}
            onChange={(e) => setBrush({ ...brush, size: e.target.value })}
          />
        </Box>
        <Box
          sx={{
            padding: 1,
            flexDirection: "inline",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Select
            sx={{ color: rgbToHex(brush.colour) }}
            defaultValue={0}
            onChange={(e) =>
              setBrush({
                ...brush,
                drawFunction: brushShapes[e.target.value as number],
              })
            }
          >
            <MenuItem sx={{ color: rgbToHex(brush.colour) }} value={0}>
              <big>●</big>
            </MenuItem>
            <MenuItem sx={{ color: rgbToHex(brush.colour) }} value={1}>
              <big>■</big>
            </MenuItem>
          </Select>
        </Box>
      </Box>
    </Box>
  );
}

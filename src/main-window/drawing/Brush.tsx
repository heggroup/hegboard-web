import { RGB } from "../../types/types";
import { DrawFunctionArgs, drawSquare } from "./DrawFunctions";

export type Brush = {
  colour: RGB;
  size: number;
  drawFunction: (args: DrawFunctionArgs) => void;
};

export const defaultBrush: Brush = {
  colour: { r: 255, g: 0, b: 0 },
  size: 5,
  drawFunction: drawSquare,
};

export function hexToRgb(colour: string): RGB {
  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(colour);
  if (result) {
    const r = parseInt(result[1], 16);
    const g = parseInt(result[2], 16);
    const b = parseInt(result[3], 16);
    return { r, g, b };
  }
  return defaultBrush.colour;
}

export function rgbToHex(colour: RGB): string {
  return (
    "#" +
    ((1 << 24) + (colour.r << 16) + (colour.g << 8) + colour.b)
      .toString(16)
      .slice(1)
  );
}

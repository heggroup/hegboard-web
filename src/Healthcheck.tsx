import {
  AssignmentLate,
  AssignmentTurnedIn,
  Pending,
} from "@mui/icons-material";
import { Tooltip } from "@mui/material";
import React, { useEffect, useState } from "react";
import config from "./Config";

interface HealthCheckResult {
  outcome: string;
  shortMessage: string;
}

export default function HealthCheck() {
  const [health, setHealth] = useState<HealthCheckResult>({
    outcome: "UNKNOWN",
    shortMessage: "Pending...",
  });

  const performHealthcheck = () => {
    fetch(`${config.baseUrl}/health`, {
      method: "GET",
    })
      .then((response) => {
        if (response.status !== 200) {
          setHealth({ outcome: "WARN", shortMessage: "HegBoard not healthy" });
        } else {
          setHealth({ outcome: "OK", shortMessage: "OK" });
        }
      })
      .catch((error) => {
        console.log(`Healthcheck error: ${error}`)
        setHealth({
          outcome: "ERROR",
          shortMessage: "Unable to reach HegBoard",
        });
      });
  };

  useEffect(() => {
    performHealthcheck();

    const task = setInterval(performHealthcheck, 5000);

    return () => {
      clearTimeout(task);
    };
  }, []);

  // TODO: Make this not fugly (colour wise)
  return (
    <Tooltip title={health.shortMessage}>
      <div>
        {health.outcome === "OK" && (
          <AssignmentTurnedIn sx={{ color: "green" }} />
        )}
        {health.outcome === "WARN" && (
          <AssignmentLate sx={{ color: "yellow" }} />
        )}
        {health.outcome === "ERROR" && <AssignmentLate sx={{ color: "red" }} />}
        {health.outcome === "UNKNOWN" && <Pending sx={{ color: "grey" }} />}
      </div>
    </Tooltip>
  );
}
